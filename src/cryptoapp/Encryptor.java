package cryptoapp;

import java.io.IOException;
import java.util.Map;

public class Encryptor extends Cryptor {

	public Encryptor(String alphabetFile, String outputFile) throws IOException {
		super(alphabetFile, outputFile);
	}

	@Override
	protected void addElementToCryptoMap(Map<String, String> cryptoMap, String line) {
		String[] lineParts = line.split(", ");
		cryptoMap.put(lineParts[0], lineParts[1]);
	}

	@Override
	protected String performLineCryptoOperation(String encryptedMessage, String messageLine) {
		for (char c : messageLine.toCharArray()) {
			String key = String.valueOf(c);
			String encryptedChar = this.cryptoMap.get(key.toUpperCase());
			if (encryptedChar == null) {
				encryptedMessage += "|ERROR: missing letter" + key.toUpperCase() + "|";
			} else {
				encryptedMessage += encryptedChar;
			}
		}
		return encryptedMessage;
	}

}
