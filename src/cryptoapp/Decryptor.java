package cryptoapp;

import java.io.IOException;
import java.util.Map;

public class Decryptor extends Cryptor {

	public Decryptor(String alphabetFile, String outputFile) throws IOException {
		super(alphabetFile, outputFile);
	}

	@Override
	protected String performLineCryptoOperation(String encryptedMessage, String messageLine) {
		String decryptedMessage = "";
		for (int i = 0; i < messageLine.length() - 31; i += 32) {
			String key = messageLine.substring(i, i + 32);
			String decryptedChar = this.cryptoMap.get(key);
			if (decryptedChar == null) {
				decryptedMessage += "|ERROR:" + key + "|";
			} else {
				decryptedMessage += decryptedChar;
			}
		}
		return decryptedMessage;
	}

	@Override
	protected void addElementToCryptoMap(Map<String, String> cryptoMap, String line) {
		String[] lineParts = line.split(", ");
		cryptoMap.put(lineParts[1], lineParts[0]);
	}

}
