package cryptoapp;

public interface Translator {

	public String translate(String textToTranslate);
	
}
