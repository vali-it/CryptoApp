package cryptoapp;

import java.io.IOException;

public class CryptoApp {

	public static void main(String[] args) throws IOException {
		String alphabetFilePath = args[0];
		String messageFilePath = args[1];
		String outputFilePath = args[2];
		
//		Encryptor myCryptor = new Encryptor(alphabetFilePath, outputFilePath);
//		myCryptor.cryptFromFile(messageFilePath);
//		myCryptor.cryptString("See on salajane tekst.");
		
		Decryptor myDecryptor = new Decryptor(alphabetFilePath, outputFilePath);
		myDecryptor.cryptFromFile(messageFilePath);
		myDecryptor.cryptString("DB90441C894B4114B3A60853CC6DC006D1E93E84944E4ACF929BB81E4D9D10B2");
		System.out.println("Encryption successful!");
		
//		Translator translator = new Cryptor(alphabetFilePath, outputFilePath);
//		System.out.println(translator.translate("text to translate"));
	}
	
}
