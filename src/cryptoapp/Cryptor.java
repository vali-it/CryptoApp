package cryptoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Cryptor implements Translator {

	protected Map<String, String> cryptoMap = new HashMap<>();
	protected String outputFile = null;
	
	public Cryptor(String alphabetFile, String outputFile) throws IOException {
		List<String> alphabetFileLines = this.readFileContents(alphabetFile);
		this.cryptoMap = this.generateCryptoMap(alphabetFileLines);
		this.outputFile = outputFile;
	}

	public void cryptFromFile(String messageFile) throws IOException {
		List<String> messageFileLines = this.readFileContents(messageFile);
		String encryptedMessage = this.encryptMessage(messageFileLines);
		this.saveEncryptedMessage(encryptedMessage);
	}
	
	public void cryptString(String messageText) throws IOException {
		List<String> messageTextList = new ArrayList<>();
		messageTextList.add(messageText);
		String encryptedMessage  = this.encryptMessage(messageTextList);
		this.saveEncryptedMessage(encryptedMessage);
	}
	
	@Override
	public String translate(String textToTranslate) {
		List<String> messageTextList = new ArrayList<>();
		messageTextList.add(textToTranslate);
		return this.encryptMessage(messageTextList);
	}

	private void saveEncryptedMessage(String encryptedMessage) throws IOException {
		Path path = Paths.get(this.outputFile);
		byte[] outputBytes = encryptedMessage.getBytes("UTF-8");
		Files.write(path, outputBytes, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
	}

	private String encryptMessage(List<String> messageFileLines) {
		String encryptedMessage = "";
		
		for (String messageLine : messageFileLines) {
			encryptedMessage += performLineCryptoOperation(encryptedMessage, messageLine);
			encryptedMessage += "\r\n";
		}
		
		return encryptedMessage;
	}

	protected abstract String performLineCryptoOperation(String encryptedMessage, String messageLine);

	protected List<String> readFileContents(String filePath) throws IOException {
		return Files.readAllLines(Paths.get(filePath));
	}
	
	protected Map<String, String> generateCryptoMap(List<String> fileLines) {
		Map<String, String> cryptoMap = new HashMap<>();
		
		for (String line : fileLines) {
			addElementToCryptoMap(cryptoMap, line);
		}
		
		return cryptoMap;
		
	}

	protected abstract void addElementToCryptoMap(Map<String, String> cMap, String line);
}
